# Hexkey Utils

Utils for working with hypercore keys without worrying if they are in hex, buffer or z-base32 format.

## Install

`npm add hexkey-utils`

## API
`key` can be either in buffer format, hexadecimal string format or z-base32 string format.

#### `asBuffer (key)`

Return the specified key as a buffer

#### `asHex (key)`
Return the specified key as hexadecimal string

#### `asZBase32 (key)`
Returns the specified key as z-base32 string

#### `getDiscoveryKey (key)`
Return the discovery key corresponding to this key, in hex format.

#### `validateKey (key)`
Throws an error if the key is not valid for a hypercore (returns true if valid)

#### `isSame (key1, key2)`
Returns true if the keys refer to the same hypercore (regardless of format), false otherwise

#### `isKey (key)`
Returns true if `key` is a valid hypercore key, false otherwise
