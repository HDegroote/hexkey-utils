const hyperCrypto = require('hypercore-crypto')
const b4a = require('b4a')
const { encode: encodeZBase32, decode: decodeZBase32 } = require('hypercore-id-encoding')

const HEXKEY_REGEX = /^[0-9a-f]{64,64}$/

function asBuffer (key) {
  return typeof key === 'string' ? decodeZBase32(key) : key
}

function asHex (key) {
  if (typeof key === 'string') {
    return key.length === 52 ? asHex(decodeZBase32(key)) : key
  }
  return b4a.toString(key, 'hex')
}

function asZBase32 (key) {
  if (typeof key === 'string') {
    return key.length === 52 ? key : asZBase32(asBuffer(key))
  }
  return encodeZBase32(key)
}

function getDiscoveryKey (key) {
  return asHex(hyperCrypto.discoveryKey(asBuffer(key)))
}

function isKey (key) {
  return HEXKEY_REGEX.test(asHex(key))
}

function validateKey (key) {
  const hexKey = asHex(key)
  if (!isKey(key)) {
    throw new Error(`Invalid key: ${hexKey}--should match ${HEXKEY_REGEX}`)
  }
  return true
}

function isSame (key1, key2) {
  return asHex(key1) === asHex(key2)
}

module.exports = {
  asBuffer,
  asHex,
  getDiscoveryKey,
  validateKey,
  isSame,
  isKey,
  asZBase32,
  HEXKEY_REGEX
}
