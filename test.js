const { expect } = require('chai')
const b4a = require('b4a')
const { encode: encodeZBase32, decode: decodeZBase32 } = require('hypercore-id-encoding')

const { asBuffer, asHex, getDiscoveryKey, validateKey, isSame, isKey, asZBase32 } = require('.')

describe('Test hexkey-utils', function () {
  const hexKey = 'a'.repeat(64)
  const bufferKey = b4a.from(hexKey, 'hex')
  const hexDiscoveryKey = '23a6a52cfe22c9f30231e933716dee66641ded8db6dbb0e6395e34dea8bacd3a'

  it('Can get hex key as buffer', function () {
    expect(asBuffer(hexKey)).to.deep.equal(bufferKey)
  })

  it('Can get buffer key as buffer', function () {
    expect(asBuffer(bufferKey)).to.deep.equal(bufferKey)
  })

  it('can get buffer as hex', function () {
    expect(asHex(bufferKey)).to.equal(hexKey)
  })

  it('can get hex key as hex', function () {
    expect(asHex(hexKey)).to.equal(hexKey)
  })

  it('can get discovery key of hex key', function () {
    expect(getDiscoveryKey(hexKey)).to.equal(hexDiscoveryKey)
  })

  it('can get discovery key of buffer key', function () {
    expect(getDiscoveryKey(bufferKey)).to.equal(hexDiscoveryKey)
  })

  it('Validates a valid hex key', function () {
    expect(validateKey(hexKey)).to.equal(true)
  })

  it('Validates a valid buffer key', function () {
    expect(validateKey(bufferKey)).to.equal(true)
  })

  it('isKey works correctly', function () {
    expect(isKey(hexKey)).to.eq(true)
    expect(isKey(bufferKey)).to.eq(true)
    expect(isKey('a'.repeat(63))).to.eq(false)
    expect(isKey(b4a.from('nope'))).to.eq(false)
  })

  describe('Throws on invalid keys', function () {
    const invalids = ['a'.repeat(63), 'a'.repeat(65), 'k' + 'a'.repeat(63), 'A'.repeat(64)]
    invalids.forEach((invalid) => {
      it('Throws on invalid key', () => {
        expect(() => validateKey(invalid)).to.throw(`Invalid key: ${invalid}`)
      })
    })
  })

  it('Identifies same keys as equal', function () {
    expect(isSame(hexKey, hexKey)).to.equal(true)
    expect(isSame(hexKey, bufferKey)).to.equal(true)
    expect(isSame(bufferKey, bufferKey)).to.equal(true)
  })

  it('Identifies different keys as not equal', function () {
    expect(isSame(hexKey, '9'.repeat(64))).to.equal(false)
  })

  it('Recognises z-base keys', function () {
    const key = 'x'.repeat(52)
    expect(isKey(key)).to.eq(true)
  })

  it('Can get the hex of a z-base keys', function () {
    const zKey = encodeZBase32(bufferKey)
    expect(asHex(zKey)).to.deep.equal(hexKey)
  })

  it('Can get the buffer of a z-base key', function () {
    const key = 'x'.repeat(52)
    expect(asBuffer(key)).to.deep.equal(decodeZBase32(key))
  })

  it('Can get a z-base key', function () {
    const zKey = encodeZBase32(bufferKey)
    expect(asZBase32(zKey)).to.equal(zKey)
    expect(asZBase32(bufferKey)).to.equal(zKey)
    expect(asZBase32(hexKey)).to.equal(zKey)
  })
})
